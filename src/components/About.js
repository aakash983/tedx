import React, { memo } from "react";
import { Link } from "react-router-dom";
import { Jumbotron, Container, Row, Col, Card } from "react-bootstrap";
import { FaGithub, FaEnvelope, FaLinkedin, FaFacebook } from "react-icons/fa";
import image from "../assets/images/ad.jpg";
import imagee from "../assets/images/sp.jpg";
import imager from "../assets/images/rss.jpg";
import imagem from "../assets/images/mrs.jpg";
import client from "../assets/images/client.png";
import client1 from "../assets/images/client2.png";
import client2 from "../assets/images/client3.jpeg";

const consultant = [
  {
    name: "Dr. Rakesh Shah",
    title: "Colorectal Surgeon",
    image: "",
    image: imager,
    description: "",
    facebook:
      "https://www.facebook.com/share/QLzehHmQiLgBr1JY/?mibextid=qi2Omg",
    email: "drrakeshshah@gmail.com.com",
    linkedin: "https://www.linkedin.com/in/drrakeshshah",
  },
  {
    name: "Megha Rani Shah",
    title: "Nursing Officer",
    image: "",
    image: imagem,
    description: "",
    facebook: "",
    email: "megharani@gmail.com",
    linkedin: "https://www.linkedin.com/in/megharani",
  },
];

const teamMembers = [
  {
    name: "Er. Aakash Deep",
    title: "CEO",
    image: "",
    //image: image,
    description: "Software Engineer",
    linkedin: "https://www.linkedin.com/in/eraakashdeep",
    email: "aakashdeep983@gmail.com.com",
    github: "https://github.com/eraakashdeep",
  },
  {
    name: "Er. Shiwangi Patra",
    title: "Co-Founder",
    image: "",
    //image: imagee,
    description: "Civil Engineer",
    linkedin: "https://www.linkedin.com/in/shiwangi-patra-0975ab208/",
    email: "pshiwangi32@gmail.com",
    github: "https://github.com/shiwangi-patra",
  },
  {
    name: "Er. Karthick Sharma",
    title: "Sales & Marketing",
    image: "",
    //image: imagee,
    description: "IT Engineer",
    linkedin: "https://www.linkedin.com/in/Karthick-sharma",
    email: "karthicksharma@gmail.com",
    github: "https://github.com/karthick-sharma",
  },
];

const clients = [
  {
    name: "Apollo",
    logo: client,
  },
  {
    name: "Maruthi Medicals",
    logo: client1,
  },
  {
    name: "Balaji Pharma",
    logo: client2,
  },
  // Add more clients as needed
];
function About() {
  return (
    <Container>
      <h1 className="my-5 text-center">About Us</h1>
      <p>
        Online medicine delivery services represent a significant advancement in
        the biopharmaceutical industry, enhancing patient access to medications
        and improving overall healthcare delivery. By integrating robust systems
        for regulatory compliance, security, and customer satisfaction, these
        services offer a convenient and efficient solution for modern healthcare
        needs.
      </p>
      <div style={{ marginTop: "10px" }}>
        <h2 className="my-5 text-center">Meet our Advisors and Consultants</h2>
        <Row>
          {consultant.map((member, index) => (
            <Col key={index} md={4} className="mb-4">
              <Card>
                <Card.Img
                  variant="top"
                  style={{
                    inset: "0px",
                    boxSizing: "border-box",
                    padding: "0px",
                    display: "block",
                    margin: "auto",
                    border: "none",
                    objectFit: "",
                    minHeight: "100%",
                    minWidth: "100%",
                    maxHeight: "100%",
                    maxWidth: "100%",
                  }}
                  src={member.image}
                  width="100%"
                  height="200px"
                  alt={member.name}
                />
                <Card.Body>
                  <Card.Title>{member.name}</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">
                    {member.title}
                  </Card.Subtitle>
                  <Card.Text>{member.description}</Card.Text>
                  <Card.Link
                    href={member.facebook}
                    target="_blank"
                    className="me-3"
                  >
                    <FaFacebook />
                  </Card.Link>
                  <Card.Link href={`mailto:${member.email}`} className="me-3">
                    <FaEnvelope />
                  </Card.Link>
                  <Card.Link
                    href={member.linkedin}
                    target="_blank"
                    className="me-3"
                  >
                    <FaLinkedin />
                  </Card.Link>
                  <Card.Link
                    href={member.github}
                    target="_blank"
                    className="me-3"
                  >
                    <FaGithub />
                  </Card.Link>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </div>
      <div style={{ marginTop: "10px" }}>
        <h2 className="my-5 text-center">Meet our Leadership Team</h2>
        <Row>
          {teamMembers.map((member, index) => (
            <Col key={index} md={4} className="mb-4">
              <Card>
                <Card.Img
                  variant="top"
                  style={{
                    inset: "0px",
                    boxSizing: "border-box",
                    padding: "0px",
                    display: "block",
                    margin: "auto",
                    border: "none",
                    objectFit: "",
                    minHeight: "100%",
                    minWidth: "100%",
                    maxHeight: "100%",
                    maxWidth: "100%",
                  }}
                  src={member.image}
                  width="100%"
                  height="200px"
                  alt={member.name}
                />
                <Card.Body>
                  <Card.Title>{member.name}</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">
                    {member.title}
                  </Card.Subtitle>
                  <Card.Text>{member.description}</Card.Text>
                  <Card.Link
                    href={member.linkedin}
                    target="_blank"
                    className="me-3"
                  >
                    <FaLinkedin />
                  </Card.Link>
                  <Card.Link href={`mailto:${member.email}`} className="me-3">
                    <FaEnvelope />
                  </Card.Link>
                  <Card.Link
                    href={member.github}
                    target="_blank"
                    className="me-3"
                  >
                    <FaGithub />
                  </Card.Link>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </div>

      <div style={{ marginTop: "20px" }}>
        <h2 className="my-5 text-center">Our Clients</h2>
        <Row>
          {clients.map((client, index) => (
            <Col key={index} md={4} className="mb-4 text-center">
              <Card>
                <Card.Img
                  variant="top"
                  style={{
                    inset: "0px",
                    boxSizing: "border-box",
                    padding: "0px",
                    display: "block",
                    margin: "auto",
                    border: "none",
                    objectFit: "",
                    minHeight: "100%",
                    minWidth: "100%",
                    maxHeight: "100%",
                    maxWidth: "100%",
                  }}
                  src={client.logo}
                  width="100%"
                  height="200px"
                  alt={client.name}
                />
                <Card.Body>
                  <Card.Title>{client.name}</Card.Title>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </div>
    </Container>
  );
}
export default memo(About);
