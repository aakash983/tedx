import React, { memo } from "react";
import PropTypes from "prop-types";
import { Col, Jumbotron } from "react-bootstrap";
import { CenterToScreen } from "./hoc";

const JumbotronWrapper = (props) => {
  return (
    <Col {...props.col}>
      <Jumbotron>
        <h1>{props.title}</h1>
        <p>{props.description}</p>
        <div style={{ color: "#0056b3" }}>{props.children}</div>
      </Jumbotron>
    </Col>
  );
};

JumbotronWrapper.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  col: PropTypes.object,
};

JumbotronWrapper.defaultProps = {
  description: `This Feature will be available soon...`,
  col: { md: "6" },
};

export default memo(CenterToScreen(JumbotronWrapper));
