import React, { memo } from "react";
import { TopNav } from "./common";

const navOptions = [
  { title: "Login", path: "/login" },
  { title: "Register", path: "/register" },
  { title: "Contact Us", path: "/contactus" },
  { title: "About", path: "/about" },
];

function LandingPage() {
  return (
    <div className="bgimg w3-display-container w3-animate-opacity w3-text-white">
      <TopNav routes={navOptions} />
      <div className="w3-display-middle">
        <h1 className="w3-jumbo w3-animate-top">DEEP TEDX</h1>
        <hr
          className="w3-border-grey"
          style={{ margin: "auto", width: "40%" }}
        />
        <p className="w3-large w3-center">
          Online Medicine Healthcare Delivery Services.
        </p>
      </div>
      <div className="w3-display-bottomleft w3-padding-large">
        <footer>
          <p type="secondary" ellipsis style={{ textAlign: "center" }}>
            &copy; {new Date().getFullYear()} Developed by Aakash Deep. All
            rights reserved.
          </p>
        </footer>
      </div>
    </div>
  );
}

export default memo(LandingPage);
