import React, { memo } from "react";
import { Link } from "react-router-dom";
import { Col, Jumbotron, Card, Button, Form } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  FaPhone,
  FaWhatsapp,
  FaEnvelope,
  FaLinkedin,
  FaInstagram,
  FaFacebook,
  FaTwitter,
  FaYoutube,
} from "react-icons/fa";

function ContactUs() {
  return (
    <div className="d-flex justify-content-center mt-5">
      <Card style={{ width: "22rem" }}>
        <Card.Body>
          <Card.Title>Contact Us</Card.Title>
          <Card.Text>
            Feel free to reach out through any of the following methods:
          </Card.Text>
          <Button variant="primary" className="d-block w-100 mb-2">
            <FaPhone /> +91 9663043640
          </Button>
          <Button variant="success" className="d-block w-100 mb-2">
            <FaWhatsapp /> WhatsApp
          </Button>
          <Button variant="danger" className="d-block w-100 mb-2">
            <FaEnvelope /> deeptedx.team@gmail.com
          </Button>
          <Button
            href="https://www.linkedin.com/company/deeptedx/"
            target="_blank"
            variant="info"
            className="d-block w-100 mb-2"
          >
            <FaLinkedin /> LinkedIn
          </Button>
          <Button
            href="https://www.instagram.com/deep_tedx?igsh=cmJiM3p3ODh6MjQw"
            target="_blank"
            variant="warning"
            className="d-block w-100 mb-2"
          >
            <FaInstagram /> Instagram
          </Button>
          <Button
            href="https://www.facebook.com/share/FQDDhT5Qr7dHyw5Y/?mibextid=qi2Omg"
            target="_blank"
            variant="primary"
            className="d-block w-100 mb-2"
          >
            <FaFacebook /> Facebook
          </Button>
          <Button
            href="https://x.com/eraakashdeep?t=o8E99eumVmBcwY2MTtotrg&s=09"
            target="_blank"
            variant="info"
            className="d-block w-100 mb-2"
          >
            <FaTwitter /> Twitter
          </Button>
          <Button
            href="http://www.youtube.com/@DeepTedx"
            target="_blank"
            variant="danger"
            className="d-block w-100 mb-2"
          >
            <FaYoutube /> Youtube
          </Button>
          <Card.Title className="mt-4">Send us a message</Card.Title>
          <Form
          // onSubmit={handleSubmit1}
          >
            <Form.Group controlId="formName" className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter your name"
              />
            </Form.Group>

            <Form.Group controlId="formEmail" className="mb-3">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                required
                type="email"
                placeholder="Enter your email"
              />
            </Form.Group>

            <Form.Group controlId="formSubject" className="mb-3">
              <Form.Label>Subject</Form.Label>
              <Form.Control required type="text" placeholder="Enter subject" />
            </Form.Group>

            <Form.Group controlId="formMessage" className="mb-3">
              <Form.Label>Message</Form.Label>
              <Form.Control
                required
                as="textarea"
                rows={3}
                placeholder="Enter your message"
              />
            </Form.Group>

            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
        <Link style={{ textAlign: "center" }} to="/">
          Back to Dashboard
        </Link>
      </Card>
    </div>
  );
}
export default memo(ContactUs);
